package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductHouseAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductHouseAppApplication.class, args);
	}

}
