package com.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@Controller
public class ProductHouseController {

//    @GetMapping(path = "/products")
//    public List<String> getProducts() {
//        List<String> productList = Arrays.asList("Pro01", "Pro002", "ProX003");
//        return productList;
//    }

    @GetMapping(path = "/products")
    public String getProducts(Model model){
        model.addAttribute("products", Arrays.asList("iPad","iPhone","iPod"));
        return "products";
    }

    @GetMapping(path = "/logout")
    public String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "/";
    }
}
